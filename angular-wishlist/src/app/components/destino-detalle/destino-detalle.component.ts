import { Component, OnInit, InjectionToken, inject, Inject } from '@angular/core';
import { DestinosApiClient } from './../../models/destinos-api-client.model';
import { DestinoViaje } from './../../models/destino-viaje.model';
import { ActivatedRoute } from '@angular/router';
import { interval } from 'rxjs';
import { AppState } from 'src/app/app.module';
import { Store } from '@ngrx/store';

/* class DestinosApiClientViejo {
  getById(id: String): DestinoViaje {
    console.log('llamado a clase old');
    return null;
  }
}

interface AppConfig {
  apiEndpoint: String;
}

const APP_CONFIG_VALUE: AppConfig = {
  apiEndpoint: "mi_api_com"
};

const APP_CONFIG = new InjectionToken<AppConfig>('app.config');

class DestinosApiClientDecorated extends DestinosApiClient {
    constructor(@Inject(APP_CONFIG) private config: AppConfig, store: Store<AppState>) {
      super(store);
    }
    getById(id: String): DestinoViaje {
      console.log('llamado a clase new decorada');
      console.log('config ', this.config.apiEndpoint);
      return super.getById(id);
    }
}
 */

@Component({
  selector: 'app-destino-detalle',
  templateUrl: './destino-detalle.component.html',
  styleUrls: ['./destino-detalle.component.css'],
  providers: [ DestinosApiClient
/*     { provide: APP_CONFIG, useValue: APP_CONFIG_VALUE},
    { provide: DestinosApiClient, useClass: DestinosApiClientDecorated},
    { provide: DestinosApiClientViejo, useExisting: DestinosApiClient}, */
  ]
})

export class DestinoDetalleComponent implements OnInit {
  destino: DestinoViaje;
  
  constructor(private route: ActivatedRoute, private destinosApiCliente: DestinosApiClient) { }

  ngOnInit() {
    let id = this.route.snapshot.paramMap.get('id');
    this.destino = this.destinosApiCliente.getById(id);
    }

}
